// *Displaying entered number with different manipulators like setbase, setw,
// setprecision etc.

#include <iostream>

int
main (int argc, char **argv, char **envp)
{
  long number;

  std::cout << "Enter number: ";
  std::cin >> number;

  std::cout << "In decimal: " << std::dec << number << '\n';
  std::cout << "In hexadecimal: 0x" << std::hex << number << '\n';
  std::cout << "In octal: " << std::oct << number << '\n';

  return 0;
}