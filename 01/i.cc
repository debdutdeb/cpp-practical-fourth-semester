// Programs to input & output data (Simple programs).

#include <iostream>

int
main (int argc, char **argv, char **envp)
{
  std::string name;
  short age;

  std::cout << "Name: ";
  std::cin >> name;
  std::cout << "Age: ";
  std::cin >> age;

  std::cout << "Your name is " << name << '\n';
  std::cout << "Your age is " << age << std::endl;

  return 0;
}