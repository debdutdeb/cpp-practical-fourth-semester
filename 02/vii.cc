/*

Write an OOP in C++ to add, subtract and multiplication of
two matrices of size 3X3.

*/

#include <iostream>
#include <tuple>
#include <vector>

template <typename T> class Matrix
{
private:
  unsigned m_row, m_column;
  std::vector<std::vector<T> > m_data;

public:
  Matrix (std::vector<std::vector<T> > data) : m_data (data)
  {
    m_row = m_data.size ();
    m_column = m_data[0].size ();
  }

  friend std::ostream &
  operator<< (std::ostream &os, const Matrix &matrice)
  {
    os << "[ ";
    for (auto it = matrice.m_data.begin (); it != matrice.m_data.end (); it++)
      {
        for (T element : *it)
          {
            os << element << ' ';
          }
        if (it + 1 != matrice.m_data.end ())
          {
            os << "\n  ";
          }
      }
    os << "]\n";
    return os;
  };

  friend Matrix
  operator+ (const Matrix &lhs, const Matrix &rhs)
  {
    std::vector<std::vector<T> > data;
    using MatrixConstIter =
        typename std::vector<std::vector<T> >::const_iterator;
    for (MatrixConstIter lhs_iter = lhs.m_data.begin (),
                         rhs_iter = rhs.m_data.begin ();
         lhs_iter != lhs.m_data.end () && rhs_iter != rhs.m_data.end ();
         lhs_iter++, rhs_iter++)
      {
        {
          std::vector<T> row;
          for (std::size_t size = 0; size < (*lhs_iter).size (); size++)
            {
              row.push_back ((*lhs_iter)[size] + (*rhs_iter)[size]);
            }
          data.push_back (row);
        }
      }
    return Matrix{ data };
  }

  friend Matrix
  operator- (const Matrix &lhs, const Matrix &rhs)
  {
    std::vector<std::vector<T> > data;
    using MatrixConstIter =
        typename std::vector<std::vector<T> >::const_iterator;
    for (MatrixConstIter lhs_iter = lhs.m_data.begin (),
                         rhs_iter = rhs.m_data.begin ();
         lhs_iter != lhs.m_data.end () && rhs_iter != rhs.m_data.end ();
         lhs_iter++, rhs_iter++)
      {
        {
          std::vector<T> row;
          for (std::size_t size = 0; size < (*lhs_iter).size (); size++)
            {
              row.push_back ((*lhs_iter)[size] - (*rhs_iter)[size]);
            }
          data.push_back (row);
        }
      }
    return Matrix{ data };
  }
};

inline void
seperator ()
{
  std::cout << "====================\n";
}

int
main ()
{
  Matrix<int> matrice1{ { { 1, 2, 3 }, { 2, 3, 4 }, { 9, 8, 5 } } };
  Matrix<int> matrice2{ { { 6, 9, 1 }, { 5, 8, 1 }, { 3, 6, 15 } } };

  std::cout << "Matrix one: \n" << matrice1 << '\n';
  std::cout << "Matrix two: \n" << matrice2 << '\n';
  seperator ();

  std::cout << matrice1 + matrice2 << std::endl;
  seperator ();
  std::cout << matrice1 - matrice2 << std::endl;

  return 0;
}