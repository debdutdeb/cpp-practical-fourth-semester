/*

To create a simple class with three different member data (
int, float and char). Write member function to insert data
into those members and display them accordingly.

*/

#include <iostream>

class SimpleClass
{
public:
  int m_int;
  float m_float;
  char m_char;

  void set_int (int);
  void set_float (float);
  void set_char (char);

  friend std::ostream &operator<< (std::ostream &, const SimpleClass &);
};

void
SimpleClass::set_int (int data)
{
  m_int = data;
}

void
SimpleClass::set_float (float data)
{
  m_float = data;
}

void
SimpleClass::set_char (char data)
{
  m_char = data;
}

std::ostream &
operator<< (std::ostream &output_stream, const SimpleClass &instance)
{
  output_stream << "Integer: " << instance.m_int
                << "\nFloat: " << instance.m_float
                << "\nCharacter: " << instance.m_char;
  return output_stream;
}

template <typename T>
T
get_input (std::string prompt = "")
{
  T variable;
  std::cout << prompt;
  std::cin >> variable;
  return variable;
}

void
seperator ()
{
  std::cout << "=================" << std::endl;
}

int
main (int argc, char **argv, char **envp)
{

  SimpleClass object{ get_input<int> ("Enter Integer: "),
                      get_input<float> ("Enter Float: "),
                      get_input<char> ("Enter Character: ") };

  seperator ();
  std::cout << "Printing SimpleClass object: \n" << object << std::endl;
  seperator ();

  object.set_int (get_input<int> ("Enter a different int value for object: "));
  object.set_float (
      get_input<float> ("Enter a different float value for object: "));
  object.set_char (
      get_input<char> ("Enter a different char value for object: "));

  seperator ();
  std::cout << "SimpleClass object after setting different int, float and "
               "char values: \n"
            << object << std::endl;

  return 0;
}