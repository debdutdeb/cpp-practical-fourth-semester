/*

Create a student class with data members as roll, name and
marks with respective data types as int, chars and float. Now
create n objects of student type and insert data into those
objects. Display the student information who got the highest
mark.

*/

#include <algorithm>
#include <cstring>
#include <iostream>
#include <vector>

class Student
{
  std::string m_name;
  int m_roll;
  float m_marks;

public:
  Student (std::string, int, float);
  inline friend bool operator< (const Student &, const Student &);
  inline friend bool operator> (const Student &, const Student &);
  friend inline std::ostream &operator<< (std::ostream &, Student &);
};

Student::Student (std::string name, int roll, float marks)
    : m_name (name), m_roll (roll), m_marks (marks)
{
}

bool
operator< (const Student &lhs, const Student &rhs)
{
  return lhs.m_marks < rhs.m_marks;
}

bool
operator> (const Student &lhs, const Student &rhs)
{
  return lhs.m_marks > rhs.m_marks;
}

inline std::ostream &
operator<< (std::ostream &os, Student &student)
{
  os << "Name: " << student.m_name << "\nRoll: " << student.m_roll
     << "\nMarks: " << student.m_marks;
  return os;
}

template <typename T>
T
get_data (std::string prompt = "")
{
  T buffer;
  std::cout << prompt;
  std::cin >> buffer;
  return buffer;
}

int
main (int argc, char **argv, char **envp)
{
  std::vector<Student> students;
  for (int i = get_data<int> ("Enter the number of students: "); i > 0; i--)
    {
      std::cout << "============" << std::endl;
      students.push_back (
          Student{ get_data<std::string> ("Enter student name: "),
                   get_data<int> ("Enter student roll: "),
                   get_data<float> ("Enter student marks: ") });
    }

  std::sort (students.begin (), students.end ());

  std::cout << "============" << std::endl;
  std::cout << students.back () << std::endl;

  return 0;
}