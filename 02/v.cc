/*

To find greatest / smallest of three numbers using OOP in
C++

*/

#include <algorithm>
#include <array>
#include <iostream>

// How the hell am I supposed to implement OOP in a simple sort?!

class SortedList
{
  std::array<int, 3> m_integer_list;

public:
  SortedList (std::array<int, 3>);
  int get_smallest ();
  int get_largest ();
};

SortedList::SortedList (std::array<int, 3> list) : m_integer_list (list)
{
  std::sort (m_integer_list.begin (), m_integer_list.end ());
}

int
SortedList::get_largest ()
{
  return m_integer_list[2];
}

int
SortedList::get_smallest ()
{
  return m_integer_list[0];
}

int
get_int (std::string prompt)
{
  std::cout << prompt;
  int number;
  std::cin >> number;
  return number;
}

int
main (int argc, char **argv, char **envp)
{

  SortedList list{ { get_int ("Enter digit #1: "),
                     get_int ("Enter digit #2: "),
                     get_int ("Enter digit #3: ") } };
  std::cout << "The smallest among three is: " << list.get_smallest ()
            << std::endl;
  std::cout << "The largest among three is: " << list.get_largest ()
            << std::endl;
  return 0;
}